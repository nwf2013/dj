module dj

go 1.13

require (
	github.com/VictoriaMetrics/fastcache v1.5.4
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/coreos/etcd v3.3.18+incompatible // indirect
	github.com/coreos/go-systemd v0.0.0-20190719114852-fd7a80b32e1f // indirect
	github.com/coreos/pkg v0.0.0-20180928190104-399ea9e2e55f // indirect
	github.com/fluent/fluent-logger-golang v1.4.0
	github.com/go-redis/redis/v7 v7.0.0-beta.4
	github.com/google/uuid v1.1.1 // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/json-iterator/go v1.1.8
	github.com/modern-go/reflect2 v1.0.1
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/tinylib/msgp v1.1.0 // indirect
	github.com/valyala/fasthttp v1.6.0
	go.etcd.io/etcd v3.3.18+incompatible
	go.uber.org/zap v1.13.0 // indirect
)
