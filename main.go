package main

import (
	"os"
	"fmt"

	"dj/libs/rpc"
	"dj/libs/form"
	"dj/libs/utils"
	"dj/libs/store"
	"dj/libs/logger"
	"dj/colf/types"
	"dj/libs/session"

	"github.com/valyala/fasthttp"
)

/*
http://127.0.0.1:8500/v1/kv/dj?raw=true
https://github.com/smallnest/gosercomp/blob/master/colorgroup.colf
colf-linux  -b src -s 2048 -l 96 go colf/user.colf

http://127.0.0.1:8080/?id=1&regtime=1&username=carl&password=123456&state=2

*/
type config struct {

	Session struct {
		Addr string `json:"addr"`
		Auth string `json:"auth"`
	} `json:"session"`
	Fluent struct {
		Host string `json:"host"`
		Port int    `json:"port"`
	} `json:"fluent"`
	Addr string `json:"addr"`
}

var conf config

func Login(ctx *fasthttp.RequestCtx) {

	session.Set(ctx, "Test")
	fmt.Fprint(ctx, "Welcome!\n")
}


func Index(ctx *fasthttp.RequestCtx) {

	
	u := types.Member{}

	val, err := form.Bind(ctx, u)
	if err != nil {
		fmt.Println(err)
		return
	}

	u          = val.(types.Member)
	u.Regtime  = uint32(ctx.Time().Unix())
	u.Id       = utils.Cputicks()

	fmt.Println(u)
	
	d, err := u.MarshalBinary()
	if err != nil {
		fmt.Println(err)
		return
	}

	res := rpcx.Call("Insert", d)

	fmt.Fprintf(ctx, "{data:'%s', state:%t}\n", res.Data, res.State)
	//fmt.Fprint(ctx, "Welcome!\n")
	/*
	session.Set(ctx, "Test")

	data := map[string]string{
		"foo":  "bar",
		"hoge": "hoge",
	}
	  
	logger.Notice(data)
	fmt.Fprint(ctx, "Welcome!\n")
	*/
}

func Hello(ctx *fasthttp.RequestCtx) {

	val, err := session.Get(ctx)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Fprintf(ctx, "hello, %s-%s!\n", ctx.UserValue("name"), val)
}


func main() {

	
	
	fmt.Println("os.Args = ", os.Args[1])
	parseConfig(os.Args[1], os.Args[2])

	logger.New(conf.Fluent.Host, conf.Fluent.Port)
	rpcx.NewClient("http://127.0.0.1:8181/")
	store.New()

	// store.Set([]byte("key"), []byte("value"))
	// val := store.Get(nil, []byte("key"))
	// fmt.Println(string(val))



	defer func () {
		logger.Close()
	}()
	
	app := NewRoute() 

	app.GET("/", Index)
	app.GET("/hello/:name", Hello)
	app.Start(":8080")
}

