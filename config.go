package main

import (
	"fmt"
	"log"
	"dj/libs/session"
	"dj/libs/discovery"
	//"go.etcd.io/etcd/clientv3"
	"github.com/json-iterator/go"
)

// https://www.cnblogs.com/jiujuan/p/10930664.html
// "172.18.223.86:2379"

var cjson = jsoniter.ConfigCompatibleWithStandardLibrary

func parseConfig(addrs, key string) {
	
	var sessionConf session.Config

	discovery.New(addrs)
	defer discovery.Close()



	resp, err := discovery.Get(key)
	if err != nil {
		log.Panic(err)
	}
	//fmt.Println("lease:", resp.Kvs)

	item := resp.Kvs[0]
	fmt.Printf("%s : %s \n", item.Key, item.Value)
		
	cjson.Unmarshal(item.Value, &conf)

	sessionConf.Addr = conf.Session.Addr
	sessionConf.Auth = conf.Session.Auth
	
	session.New(sessionConf)
	
}



