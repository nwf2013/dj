  
package main

import (
	"fmt"
	"log"
	"time"
	"runtime/debug"
	"github.com/valyala/fasthttp"
	"github.com/buaazp/fasthttprouter"
)



type route struct {
	Router *fasthttprouter.Router
}

var (
	apiTimeoutMsg = `{"flags": "1","status": "error","status_code": 403,"message": "服务器响应超时，请稍后重试","data":null}`
	apiTimeout    = time.Second * 30
)

func NewRoute() *route {

	app := new(route)
	app.Router = fasthttprouter.New()
	app.Router.PanicHandler = func(ctx *fasthttp.RequestCtx, p interface{}) {
		//进程异常处理
		err := p.(error)
		fmt.Println(err)
		debug.PrintStack()
		return
	}

	return app
}

func (this *route) GET(path string, handle fasthttp.RequestHandler) {

	this.Router.GET(path, fasthttp.TimeoutHandler(handle, apiTimeout, apiTimeoutMsg))
}

func (this *route) HEAD(path string, handle fasthttp.RequestHandler) {

	this.Router.HEAD(path, fasthttp.TimeoutHandler(handle, apiTimeout, apiTimeoutMsg))
}

func (this *route) OPTIONS(path string, handle fasthttp.RequestHandler) {

	this.Router.OPTIONS(path, fasthttp.TimeoutHandler(handle, apiTimeout, apiTimeoutMsg))
}

func (this *route) POST(path string, handle fasthttp.RequestHandler) {

	this.Router.POST(path, fasthttp.TimeoutHandler(handle, apiTimeout, apiTimeoutMsg))
}

func (this *route) PUT(path string, handle fasthttp.RequestHandler) {
	
	this.Router.PUT(path, fasthttp.TimeoutHandler(handle, apiTimeout, apiTimeoutMsg))
}

func (this *route) PATCH(path string, handle fasthttp.RequestHandler) {
	
	this.Router.PATCH(path, fasthttp.TimeoutHandler(handle, apiTimeout, apiTimeoutMsg))
}

func (this *route) DELETE(path string, handle fasthttp.RequestHandler) {

	this.Router.DELETE(path, fasthttp.TimeoutHandler(handle, apiTimeout, apiTimeoutMsg))
}


func (this *route) Start(addr string) {

	srv := &fasthttp.Server{
		Handler: this.Router.Handler, //全局路由日志
		Name:    "DJ",
	}
	log.Fatal(srv.ListenAndServe(addr))
	//log.Fatal(fasthttp.ListenAndServe(addr, this.Router.Handler))
}
